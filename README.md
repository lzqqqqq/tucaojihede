# 吐槽吧

## Big Data

### 介绍

这是大数据的期末项目，我们小组做的是《吐槽吧》APP平台，主要为城市高压人群提供一个可以随心吐槽的平台，采用线上线下结合的方式，在线为用户
提供吐槽空间，匹配契合度高的聊天好友，以及为他们提供心理医生在线咨询服务，线下为用户挑选推荐合适的放松场所，以便他们放松身心。

### 参与贡献

1. 周三12-14 B2小组成员成为本仓库项目合作开发者
2. 小组成员：陈菲儿，吴慧晶，陈婷，许钰然，刘子琦，黄梓珊，孔晓桐
3. 各小组成员分工合作，各自新建markdown文件，提出他们的想法，组长菲儿成为merge者，其他组员提出想法并进行pull request。
4. 最后进行合并，形成最终的项目成果

####    项目贡献情况

>    陈菲儿、黄梓珊

- 贡献说明：陈菲儿同学提出了前三点想法，黄梓珊同学提出了后三点想法

- [产品原型选择](https://gitee.com/neria/tucaojihede/blob/master/%E4%BA%A7%E5%93%81%E5%8E%9F%E5%9E%8B%E7%9A%84%E9%80%89%E6%8B%A9.md)

>    陈菲儿

- [项目策划方案](https://gitee.com/neria/tucaojihede/blob/master/%E9%A1%B9%E7%9B%AE%E7%AD%96%E5%88%92%E6%96%B9%E6%A1%88.md)

>    陈婷、孔晓桐

- 贡献说明：孔晓桐同学提交了数据伦理实验室的第一至第三点想法；陈婷同学提交了数据伦理实验室的第四至第八点想法

- [数据伦理实验室](https://gitee.com/neria/tucaojihede/blob/master/%E6%95%B0%E6%8D%AE%E4%BC%A6%E7%90%86%E5%AE%9E%E9%AA%8C%E5%AE%A4.md)

>    吴慧晶

- [把城市发展问题变成数据问题](https://gitee.com/neria/tucaojihede/blob/master/%E6%8A%8A%E5%9F%8E%E5%B8%82%E5%8F%91%E5%B1%95%E9%97%AE%E9%A2%98%E5%8F%98%E6%88%90%E6%95%B0%E6%8D%AE%E9%97%AE%E9%A2%98.md)

>    许钰然、刘子琦

- 贡献说明：许钰然同学贡献前三点想法，刘子琦同学贡献后两点想法

- [项目挑战与解决方案](https://gitee.com/neria/tucaojihede/blob/master/%E9%A1%B9%E7%9B%AE%E6%8C%91%E6%88%98%E4%B8%8E%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88.md)

>    许钰然

- [README](https://gitee.com/neria/tucaojihede/blob/master/README.md)

>    贡献者commit页

- [贡献者们](https://gitee.com/neria/tucaojihede/contributors?ref=master)
